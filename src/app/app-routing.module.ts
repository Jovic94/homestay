import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
	{
		path: 'auth',
		loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
	},
	{
		path: 'client',
		loadChildren: () =>
			import('./clients/client-tabs/client-tabs.module').then(
				m => m.ClientTabsPageModule
			)
	},
	{
		path: 'host',
		loadChildren: () =>
			import('./hosts/host-tabs/host-tabs.module').then(
				m => m.HostTabsPageModule
			)
	},
	{
		path: '',
		redirectTo: '/auth',
		pathMatch: 'full'
	},
  { path: 'property-details', loadChildren: './clients/explore/property-details/property-details.module#PropertyDetailsPageModule' }
];
@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
	],
	exports: [RouterModule]
})
export class AppRoutingModule {}
