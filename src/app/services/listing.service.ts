import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class ListingService {
	constructor(private afs: AngularFirestore) {}

	createListing(listing) {
		return this.afs.collection('listings').add(listing);
	}

	updateListing(id, gallery) {
		return this.afs
			.collection('listings')
			.doc(id)
			.update({ gallery: gallery });
	}

	getUserListings(uid) {
		const snapshot = this.afs
			.collection('listings', ref => ref.where('uid', '==', uid))
			.snapshotChanges()
			.pipe(
				map(actions => {
					return actions.map(a => {
						const data = a.payload.doc.data();
						const id = a.payload.doc.id;
						return { id, ...data };
					});
				})
			);
		return snapshot;
	}

	getListing(lid) {
		const snapshot = this.afs
			.collection('listings')
			.doc(lid)
			.valueChanges();
		return snapshot;
	}

	getAllListings() {
		const snapshot = this.afs
			.collection('listings')
			.snapshotChanges()
			.pipe(
				map(actions => {
					return actions.map(a => {
						const data = a.payload.doc.data();
						const id = a.payload.doc.id;
						return { id, ...data };
					});
				})
			);
		return snapshot;
	}
}
