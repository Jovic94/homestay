import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
@Injectable({
	providedIn: 'root'
})
export class AuthService {
	constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore) {}

	/**
	 * @description :: Check if user exists
	 */
	async checkUser(uid) {
		return this.afs
			.collection('users')
			.doc(uid)
			.valueChanges() as Observable<any>;
	}

	/**
	 * @description :: Login With Facebook
	 */
	facebookLogin() {
		return this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());
	}

	/**
	 * @description :: Login with Email and Password
	 */
	emailLogin(email, password) {
		return this.afAuth.auth.signInWithEmailAndPassword(email, password);
	}

	/**
	 * @description :: Email and password sign up
	 */
	emailSignUp(email, password) {
		return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
	}

	/**
	 * @description :: Create new user
	 */
	createUser(user) {
		const snapshot = this.afs
			.collection('users')
			.doc(user.uid)
			.set(user);
		return snapshot;
	}

	/**
	 * @description :: Update profile info for user
	 */
	updateProfile(profile) {
		const snapshot = this.afs
			.collection('users')
			.doc(profile.uid)
			.update(profile);
		return snapshot;
	}

	/**
	 * @description :: User Profile
	 */
	getUserProfile(uid) {
		const snapshot = this.afs
			.collection('users')
			.doc(uid)
			.valueChanges();
		return snapshot;
	}

	async logout() {
		localStorage.removeItem('user');
		return await this.afAuth.auth.signOut();
	}

	/**
	 * @desription :: Switch between host and user
	 */
	swithBetween(val) {
		return this.afs
			.collection('users')
			.doc(val.uid)
			.update(val);
	}

	resetPassword(email: string) {
		return this.afAuth.auth.sendPasswordResetEmail(email);
	}
}
