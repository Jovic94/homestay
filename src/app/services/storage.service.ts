import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
@Injectable({
	providedIn: 'root'
})
export class StorageService {
	constructor(private nativeStorage: NativeStorage) {}

	async setUser(user) {
		return this.nativeStorage.setItem('user', user);
	}

	async getUser() {
		return this.nativeStorage.getItem('user');
	}

	async setState(state) {
		const res = await this.nativeStorage.setItem('state', state);
		return res;
	}

	async getState() {
		const state = this.nativeStorage.getItem('state');
		return state;
	}

	async clearStorage() {
		this.nativeStorage.remove('user');
		return this.nativeStorage.remove('state');
	}
}
