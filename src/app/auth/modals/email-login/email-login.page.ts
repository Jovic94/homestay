import { Component, OnInit } from '@angular/core';
import {
	ModalController,
	ToastController,
	LoadingController,
	NavController
} from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from 'src/app/services/auth.service';
import { User } from '../../../models/users';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';

@Component({
	selector: 'app-email-login',
	templateUrl: './email-login.page.html',
	styleUrls: ['./email-login.page.scss']
})
export class EmailLoginPage implements OnInit {
	email: string;
	password: string;
	user: User = {};
	loading: any;

	constructor(
		private modalCtrl: ModalController,
		private authService: AuthService,
		private toastCtrl: ToastController,
		private loadingCtrl: LoadingController,
		private router: Router,
		private storageService: StorageService
	) {}

	ngOnInit() {}

	/**
	 * @description :: Dismiss Email and Password modal window
	 */
	async dismissModal(val) {
		val === 'reset' ? this.modalCtrl.dismiss(val) : this.modalCtrl.dismiss();
	}

	/**
	 * @description :: Email and Password Sign In
	 */
	onEmailSignIn() {
		this.loader('Signing In ...');
		this.authService
			.emailLogin(this.email, this.password)
			.then(res => {
				// Check if the user exists in the db
				this.authService.checkUser(res.user.uid).then(callback => {
					callback.subscribe(user => {
						if (user.status === 'Active') {
							this.storageService.setUser(JSON.stringify(user));
							this.storageService.setState('guest');
							this.loading.dismiss();
							this.dismissModal(undefined);
							this.router.navigateByUrl('/client/client-tabs/find', {
								skipLocationChange: true
							});
						}
					});
				});
			})
			.catch(err => {
				if (err.code === 'auth/user-not-found') {
					this.loading.dismiss();
					this.dismissModal(undefined);
					this.toastError(
						'Account is missing',
						'Kindly proceed to create an account before you continue'
					);
				} else if (err.code === 'auth/wrong-password') {
					this.loading.dismiss();
					this.toastError('Wrong Password', err.message);
				}
			});
	}

	/**
	 * @description :: Toastr Function initialization
	 */
	async toastError(header, err) {
		const toast = await this.toastCtrl.create({
			header: header,
			message: err,
			position: 'bottom',
			duration: 5000
		});
		return toast.present();
	}

	/**
	 * @description :: Initiate Loader function
	 */
	async loader(message) {
		this.loading = await this.loadingCtrl.create({
			spinner: 'crescent',
			message: message,
			translucent: true,
			cssClass: 'custom-class custom-loading'
		});
		return await this.loading.present();
	}
}
