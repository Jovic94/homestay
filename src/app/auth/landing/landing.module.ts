import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { LandingPage } from './landing.page';
import { Facebook } from '@ionic-native/facebook/ngx';
import { EmailLoginPage } from '../modals/email-login/email-login.page';
import { EmailLoginPageModule } from '../modals/email-login/email-login.module';

const routes: Routes = [
  {
    path: '',
    component: LandingPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    EmailLoginPageModule
  ],
  declarations: [LandingPage],
  providers: [Facebook]
})
export class LandingPageModule {}
