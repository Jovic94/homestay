import { Component, OnInit, ViewChild } from '@angular/core';
import {
	IonSlides,
	Platform,
	NavController,
	ModalController
} from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import * as firebase from 'firebase/app';
import { EmailLoginPage } from '../modals/email-login/email-login.page';
import { Router } from '@angular/router';

@Component({
	selector: 'app-landing',
	templateUrl: './landing.page.html',
	styleUrls: ['./landing.page.scss'],
	providers: [Facebook]
})
export class LandingPage implements OnInit {
	@ViewChild('mySlider', { static: true }) slider: IonSlides;
	currentIndex: any;
	providerFb: firebase.auth.FacebookAuthProvider;

	constructor(
		private authService: AuthService,
		private fb: Facebook,
		public platform: Platform,
		private router: Router,
		private modalCtrl: ModalController
	) {
		this.currentIndex = 0;
		this.providerFb = new firebase.auth.FacebookAuthProvider();
	}

	ngOnInit() {}

	async onSlideChanged() {
		this.currentIndex = await this.slider.getActiveIndex();
		if (this.currentIndex === 3) {
			this.slider.lockSwipes(true);
		}
	}

	async skip() {
		this.slider.slideTo(3, 1000).then(() => {
			this.slider.lockSwipeToPrev(true);
		});
	}

	loginWithFacebook() {
		// this.navCtrl.navigateRoot('/client/client-tabs/find');
		// if (this.platform.is('cordova')) {
		// 	console.log('PLateforme cordova');
		// 	this.fb
		// 		.login(['public_profile', 'email'])
		// 		.then(async (res: FacebookLoginResponse) => {
		// 			const fbCredential = await firebase.auth.FacebookAuthProvider.credential(
		// 				res.authResponse.accessToken
		// 			);
		// 			firebase
		// 				.auth()
		// 				.signInWithCredential(fbCredential)
		// 				.then(success => {
		// 					console.log('Info Facebook: ' + JSON.stringify(success));
		// 				})
		// 				.catch(error => {
		// 					console.log(error);
		// 					// Toast the error
		// 				});
		// 		})
		// 		.catch(e => console.log('Error logging into Facebook', e));
		// } else {
		// 	console.log('PLateforme Web');
		// }
	}

	loginWithGoogle() {
		// this.navCtrl.navigateRoot('/client/client-tabs/find');
	}

	async loginWithEmailAndPassword() {
		const modal = await this.modalCtrl.create({
			component: EmailLoginPage
		});

		modal.onDidDismiss().then(detail => {
			if (detail.data !== null && detail.data !== undefined) {
				// redirect to component for completing profile
				this.router.navigate(['/auth/password-reset']);
			}
		});
		return await modal.present();
	}
}
