import { Component, OnInit, ViewChild } from '@angular/core';
import * as firebase from 'firebase';
import { Subscription } from 'rxjs';
import {
	ActionSheetController,
	LoadingController,
	ToastController
} from '@ionic/angular';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/users';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { NgForm } from '@angular/forms';
import { StorageService } from 'src/app/services/storage.service';

@Component({
	selector: 'app-signup',
	templateUrl: './signup.page.html',
	styleUrls: ['./signup.page.scss'],
	providers: [Camera]
})
export class SignupPage implements OnInit {
	user: User = {};
	paymentMethod = '';
	myPhotosRef: any;
	subscription: Subscription;
	loading: any;
	password: any;
	password_confirm: any;

	@ViewChild('signup', { static: true }) signupForm: NgForm;
	constructor(
		private actionSheetCtrl: ActionSheetController,
		private loadingCtrl: LoadingController,
		private camera: Camera,
		private afStorage: AngularFireStorage,
		private afAuth: AngularFireAuth,
		private authService: AuthService,
		private toastCtrl: ToastController,
		private router: Router,
		private storageService: StorageService
	) {
		this.myPhotosRef = firebase.storage().ref('/ProfilePictures');
	}

	ngOnInit() {}

	async uploadPic() {
		const actionSheet = await this.actionSheetCtrl.create({
			buttons: [
				{
					text: 'Load from gallery',
					handler: () => {
						this.loadImage(this.camera.PictureSourceType.PHOTOLIBRARY);
					}
				},
				{
					text: 'Take a photo',
					handler: () => {
						this.loadImage(this.camera.PictureSourceType.CAMERA);
					}
				},
				{
					text: 'Cancel',
					role: 'cancel'
				}
			]
		});
		await actionSheet.present();
	}

	private loadImage(selectedSourceType: number) {
		const cameraOptions: CameraOptions = {
			sourceType: selectedSourceType,
			destinationType: this.camera.DestinationType.DATA_URL,
			quality: 100,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE,
			correctOrientation: true
		};
		this.camera.getPicture(cameraOptions).then(imageData => {
			if (imageData != null) {
				// Do with the image data what you want.
				this.user.avatar_url = 'data:image/jpeg;base64,' + imageData;
				// this.upload(this.user.avatar_url);
			}
		});
	}

	async upload(pic) {
		// this.loader('Uploading profile picture ...');
		const filename = this.user.uid;
		const imageRef = this.myPhotosRef.child(`/${filename}.jpg`);
		imageRef
			.putString(pic, firebase.storage.StringFormat.DATA_URL)
			.then(async snapshot => {
				// Do something here when the data is succesfully uploaded!
				const pictures = firebase
					.storage()
					.ref(`/ProfilePictures/${filename}.jpg`);
				pictures.getDownloadURL().then(async url => {
					this.user.avatar_url = url;
					// this.loading.dismiss();
					this.toastInfo('Success', 'Profile photo uploaded successfuly');
				});
			});
	}

	/**
	 * @description :: Sign Up User
	 */
	async onSignUp() {
		this.loader('Signing up ...');
		if (this.signupForm.form.valid) {
			this.authService
				.emailSignUp(this.user.email, this.password)
				.then(res => {
					// Create user then proceed to profile page
					this.user.uid = res.user.uid;
					this.user.email = res.user.email;
					this.user.status = 'Active';
					this.user.is_host = false;
					this.user.created_at = new Date().toDateString();
					this.authService.createUser(this.user).then(async () => {
						this.authService.checkUser(this.user.uid).then(callback => {
							callback.subscribe(user => {
								if (user.status === 'Active') {
									this.storageService.setUser(JSON.stringify(user));
									this.storageService.setState('guest');
									this.loading.dismiss();
									this.router.navigateByUrl('/client/client-tabs/find', {
										skipLocationChange: true
									});
								}
							});
						});
					});
				})
				.catch(err => {
					if (err.code === 'auth/email-already-in-use') {
						this.loading.dismiss();
						this.toastInfo('Account Exists!', err.message);
					} else if (err.code === 'auth/weak-password') {
						this.loading.dismiss();
						this.toastInfo('Weak Password', err.message);
					}
				});
		} else {
			this.toastInfo('Invalid', 'Please fill in the required fields');
		}
	}

	async loader(message) {
		this.loading = await this.loadingCtrl.create({
			spinner: 'crescent',
			message: message,
			translucent: true,
			cssClass: 'custom-class custom-loading'
		});
		return await this.loading.present();
	}

	async toastInfo(header, info) {
		const toast = await this.toastCtrl.create({
			header: header,
			message: info,
			position: 'bottom',
			duration: 5000
		});
		return toast.present();
	}

	ionViewDidLeave() {
		this.upload(this.user.avatar_url);
	}
}
