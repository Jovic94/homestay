import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { Subscription } from 'rxjs';
import {
	ActionSheetController,
	LoadingController,
	ToastController
} from '@ionic/angular';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/users';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
	selector: 'app-complete-profile',
	templateUrl: './complete-profile.page.html',
	styleUrls: ['./complete-profile.page.scss'],
	providers: [Camera]
})
export class CompleteProfilePage implements OnInit {
	user: User = {};
	paymentMethod = '';
	myPhotosRef: any;
	subscription: Subscription;
	loading: any;
	constructor(
		private actionSheetCtrl: ActionSheetController,
		private loadingCtrl: LoadingController,
		private camera: Camera,
		private afStorage: AngularFireStorage,
		private afAuth: AngularFireAuth,
		private authService: AuthService,
		private toastCtrl: ToastController,
		private router: Router
	) {
		this.myPhotosRef = firebase.storage().ref('/ProfilePictures');
		this.afAuth.authState.subscribe(user => {
			if (user) {
				this.user.uid = user.uid;
				this.user.email = user.email;
			}
		});
	}

	ngOnInit() {}

	async uploadPic() {
		const actionSheet = await this.actionSheetCtrl.create({
			buttons: [
				{
					text: 'Load from gallery',
					handler: () => {
						this.loadImage(this.camera.PictureSourceType.PHOTOLIBRARY);
					}
				},
				{
					text: 'Take a photo',
					handler: () => {
						this.loadImage(this.camera.PictureSourceType.CAMERA);
					}
				},
				{
					text: 'Cancel',
					role: 'cancel'
				}
			]
		});
		await actionSheet.present();
	}

	private loadImage(selectedSourceType: number) {
		const cameraOptions: CameraOptions = {
			sourceType: selectedSourceType,
			destinationType: this.camera.DestinationType.DATA_URL,
			quality: 100,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE,
			correctOrientation: true
		};
		this.camera.getPicture(cameraOptions).then(imageData => {
			if (imageData != null) {
				// Do with the image data what you want.
				this.user.avatar_url = 'data:image/jpeg;base64,' + imageData;
				this.upload(this.user.avatar_url);
			}
		});
	}

	async upload(pic) {
		this.loader('Uploading profile picture ...');
		const filename = this.user.uid;
		const imageRef = this.myPhotosRef.child(`/${filename}.jpg`);
		imageRef
			.putString(pic, firebase.storage.StringFormat.DATA_URL)
			.then(async snapshot => {
				// Do something here when the data is succesfully uploaded!
				const pictures = firebase
					.storage()
					.ref(`/ProfilePictures/${filename}.jpg`);
				pictures.getDownloadURL().then(async url => {
					this.user.avatar_url = url;
					this.loading.dismiss();
					this.toastInfo('Success', 'Profile photo uploaded successfuly');
				});
			});
	}

	async saveProfile() {
		this.loader('Completing profile update ...');
		this.user.status = 'Active';
		this.user.is_host = false;
		this.user.updated_at = new Date().toDateString();
		this.authService.updateProfile(this.user).then(async res => {
			this.loading.dismiss();
			this.toastInfo('Success', 'Profile information successfuly updated');
			this.router.navigate(['/client/client-tabs/find']);
		});
	}

	async loader(message) {
		this.loading = await this.loadingCtrl.create({
			spinner: 'crescent',
			message: message,
			translucent: true,
			cssClass: 'custom-class custom-loading'
		});
		return await this.loading.present();
	}

	async toastInfo(header, info) {
		const toast = await this.toastCtrl.create({
			header: header,
			message: info,
			position: 'bottom',
			duration: 5000
		});
		return toast.present();
	}
}
