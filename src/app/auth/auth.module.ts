import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { Facebook } from '@ionic-native/facebook/ngx';

@NgModule({
  declarations: [],
  imports: [CommonModule, AuthRoutingModule],
  providers: [Facebook]
})
export class AuthModule {}
