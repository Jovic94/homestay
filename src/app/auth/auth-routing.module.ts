import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{
		path: '',
		loadChildren: () =>
			import('./landing/landing.module').then(m => m.LandingPageModule)
	},
	{
		path: 'landing',
		loadChildren: () =>
			import('./landing/landing.module').then(m => m.LandingPageModule)
	},
	{
		path: 'login',
		loadChildren: () =>
			import('./login/login.module').then(m => m.LoginPageModule)
	},
	{
		path: 'signup',
		loadChildren: () =>
			import('./signup/signup.module').then(m => m.SignupPageModule)
	},
	{
		path: 'email-login',
		loadChildren: () =>
			import('./modals/email-login/email-login.module').then(
				m => m.EmailLoginPageModule
			)
	},
	{
		path: 'complete-profile',
		loadChildren: () =>
			import('./complete-profile/complete-profile.module').then(
				m => m.CompleteProfilePageModule
			)
	},
	{
		path: 'password-reset',
		loadChildren: () =>
			import('./password-reset/password-reset.module').then(
				m => m.PasswordResetPageModule
			)
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AuthRoutingModule {}
