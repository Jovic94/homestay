import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ToastController, NavController } from '@ionic/angular';

@Component({
	selector: 'app-password-reset',
	templateUrl: './password-reset.page.html',
	styleUrls: ['./password-reset.page.scss']
})
export class PasswordResetPage implements OnInit {
	email: any;
	constructor(
		private authService: AuthService,
		private toastrCtrl: ToastController,
		private navCtrl: NavController
	) {}

	ngOnInit() {}

	resetPassword() {
		this.authService.resetPassword(this.email).then(async res => {
			const toastr = await this.toastrCtrl.create({
				duration: 4000,
				message: 'Email sent. Kindly check your email to reset your password',
				position: 'top'
			});

			toastr.present();
			return this.navCtrl.navigateBack('/auth/landing');
		});
	}
}
