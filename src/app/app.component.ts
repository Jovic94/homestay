import { Component, NgZone } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { StorageService } from './services/storage.service';

@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	styleUrls: ['app.component.scss']
})
export class AppComponent {
	constructor(
		private platform: Platform,
		private splashScreen: SplashScreen,
		private statusBar: StatusBar,
		private zone: NgZone,
		private storageService: StorageService,
		private navCtrl: NavController
	) {
		this.initializeApp();
	}

	initializeApp() {
		this.platform.ready().then(async () => {
			this.statusBar.styleBlackTranslucent();
			// Do the checks here
			const state = await this.storageService.getState();
			const user = await this.storageService.getUser();

			state === 'guest'
				? (this.splashScreen.hide(),
				  this.zone.run(async () => {
						await this.navCtrl.navigateRoot('/client/client-tabs/find');
				  }))
				: state === 'host'
				? (this.splashScreen.hide(),
				  this.zone.run(async () => {
						await this.navCtrl.navigateRoot('/host/host-tabs/listings');
				  }))
				: (this.splashScreen.hide(),
				  this.zone.run(async () => {
						await this.navCtrl.navigateRoot('/auth');
				  }));
		});
	}
}
