import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/users';
import { AngularFireAuth } from '@angular/fire/auth';
import {
	ToastController,
	NavController,
	LoadingController
} from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/services/storage.service';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.page.html',
	styleUrls: ['./profile.page.scss']
})
export class ProfilePage implements OnInit {
	user: User = {};
	loading: any;
	state: any;
	constructor(
		private afAuth: AngularFireAuth,
		private toastCtrl: ToastController,
		private router: Router,
		private authService: AuthService,
		private loadingCtrl: LoadingController,
		private storageService: StorageService,
		private navCtrl: NavController
	) {
		storageService.getUser().then(res => {
			this.user = JSON.parse(res);
			console.log(this.user);
		});
		storageService.getState().then(res => {
			this.state = res;
			console.log(this.state);
		});
	}

	async ngOnInit() {}

	getUserProfile(uid) {
		this.authService.getUserProfile(uid).subscribe(res => {
			this.user = res;
		});
	}

	/**
	 * @description :: Switching to being a user from the hosting side
	 */
	switchToUser() {
		this.loader('Switching To User');
		setTimeout(async () => {
			this.storageService.setState('guest');
			await this.loading.dismiss();
			return await this.router.navigate(['/client/client-tabs/find']);
		}, 2000);
	}

	/**
	 * @description :: Switching to being a host from the users side
	 */
	switchToHosting() {
		this.loader('Switching To Host');
		setTimeout(async () => {
			await this.storageService.setState('host');
			await this.loading.dismiss();
			return this.router.navigateByUrl('/host/host-tabs/listings', {
				skipLocationChange: true
			});
		}, 2000);
	}

	/**
	 * @description :: Before lisitng property, check the section you are in first, if host, proceed direct to listing page
	 * if user, switch to hosting 1st
	 */
	async listProperty() {
		const state = await this.storageService.getState();
		if (state === 'guest') {
			this.loader('Switching To Host');
			setTimeout(() => {
				this.user.is_host = true;
				this.authService.swithBetween(this.user).then(() => {
					this.loading.dismiss();
					this.storageService.setState('host');
					this.router.navigateByUrl('/host/host-tabs/listings/add-listing', {
						skipLocationChange: true
					});
				});
			}, 4000);
		} else {
			return this.router.navigateByUrl('/host/host-tabs/listings/add-listing', {
				skipLocationChange: true
			});
		}
	}

	/**
	 * @description :: Initialize the loader for reusabilitity withing the component
	 */
	async loader(message) {
		this.loading = await this.loadingCtrl.create({
			spinner: 'bubbles',
			message: message,
			translucent: true
		});
		return await this.loading.present();
	}

	/**
	 * @description :: Logout user from the system
	 */
	async logout() {
		this.loader('Logging out ...');
		this.authService.logout().then(() => {
			this.storageService.clearStorage().then(() => {
				this.loading.dismiss();
				this.router.navigate(['/auth']);
			});
		});
	}
}
