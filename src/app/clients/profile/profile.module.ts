import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { ProfilePage } from './profile.page';

const routes: Routes = [
	{
		path: 'profile',
		component: ProfilePage
	},
	{
		path: 'profile-edit',
		children: [
			{
				path: '',
				loadChildren: () =>
					import('./profile-edit/profile-edit.module').then(
						m => m.ProfileEditPageModule
					)
			}
		]
	},
	{
		path: 'payments',
		children: [
			{
				path: '',
				loadChildren: () =>
					import('./payment/payment.module').then(m => m.PaymentPageModule)
			}
		]
	},
	{
		path: 'notifications',
		children: [
			{
				path: '',
				loadChildren: () =>
					import('./notifications/notifications.module').then(
						m => m.NotificationsPageModule
					)
			}
		]
	},
	{
		path: 'terms-and-conditions',
		children: [
			{
				path: '',
				loadChildren: () =>
					import('./terms-conditions/terms-conditions.module').then(
						m => m.TermsConditionsPageModule
					)
			}
		]
	},
	{
		path: 'privacy-policy',
		children: [
			{
				path: '',
				loadChildren: () =>
					import('./privacy-policy/privacy-policy.module').then(
						m => m.PrivacyPolicyPageModule
					)
			}
		]
	},
	{
		path: '',
		redirectTo: 'profile'
	}
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes)
	],
	declarations: [ProfilePage]
})
export class ProfilePageModule {}
