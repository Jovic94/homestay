import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientTabsPage } from './client-tabs.page';

const routes: Routes = [
	{
		path: 'client-tabs',
		component: ClientTabsPage,
		children: [
			{
				path: 'find',
				children: [
					{
						path: '',
						loadChildren: () =>
							import('../explore/explore.module').then(m => m.ExplorePageModule)
					}
				]
			},
			{
				path: 'favs',
				children: [
					{
						path: '',
						loadChildren: () =>
							import('../favs/favs.module').then(m => m.FavsPageModule)
					}
				]
			},
			{
				path: 'profile',
				children: [
					{
						path: '',
						loadChildren: () =>
							import('../profile/profile.module').then(m => m.ProfilePageModule)
					}
				]
			},
			{
				path: '',
				redirectTo: '/client-tabs/find',
				pathMatch: 'full'
			}
		]
	},
	{
		path: '',
		redirectTo: '/client-tabs',
		pathMatch: 'full'
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ClientTabsPageRoutingModule {}
