import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ClientTabsPage } from './client-tabs.page';
import { ClientTabsPageRoutingModule } from './client-tabs.router.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ClientTabsPageRoutingModule
  ],
  declarations: [ClientTabsPage]
})
export class ClientTabsPageModule {}
