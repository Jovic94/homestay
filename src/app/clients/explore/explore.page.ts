import { Component, OnInit } from '@angular/core';
import { ListingService } from 'src/app/services/listing.service';

@Component({
	selector: 'app-explore',
	templateUrl: './explore.page.html',
	styleUrls: ['./explore.page.scss']
})
export class ExplorePage implements OnInit {
	listings: any;
	topCities = [
		{
			city: 'Nairobi',
			img: 'assets/images/nairobi.jpg'
		},
		{
			city: 'Mombasa',
			img: 'assets/images/mombasa.jpg'
		},
		{
			city: 'Kisumu',
			img: 'assets/images/kisumu.jpg'
		},
		{
			city: 'Nakuru',
			img: 'assets/images/nakuru.jpg'
		}
	];
	constructor(private listingService: ListingService) {}

	ngOnInit() {
		this.getListings();
	}

	getListings() {
		this.listingService.getAllListings().subscribe(res => {
			this.listings = res;
		});
	}
}
