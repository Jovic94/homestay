import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ExplorePage } from './explore.page';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

const routes: Routes = [
	{
		path: '',
		component: ExplorePage
	},
	{
		path: 'property-details',
		loadChildren: () =>
			import('./property-details/property-details.module').then(
				m => m.PropertyDetailsPageModule
			)
	}
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		FontAwesomeModule
	],
	declarations: [ExplorePage]
})
export class ExplorePageModule {}
