import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListingService } from 'src/app/services/listing.service';
import { ActionSheetController } from '@ionic/angular';

@Component({
	selector: 'app-property-details',
	templateUrl: './property-details.page.html',
	styleUrls: ['./property-details.page.scss']
})
export class PropertyDetailsPage implements OnInit {
	listing: any;
	listingId: any;
	constructor(
		private route: ActivatedRoute,
		private listingService: ListingService,
		private actionSheetCtrl: ActionSheetController
	) {
		this.listingId = route.snapshot.queryParamMap.get('id');
	}

	ngOnInit() {
		this.getListingDetails(this.listingId);
	}

	getListingDetails(id) {
		this.listingService.getListing(id).subscribe(res => {
			this.listing = res;
			console.log(this.listing);
		});
	}
}
