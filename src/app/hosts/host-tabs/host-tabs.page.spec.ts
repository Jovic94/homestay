import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HostTabsPage } from './host-tabs.page';

describe('HostTabsPage', () => {
  let component: HostTabsPage;
  let fixture: ComponentFixture<HostTabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HostTabsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
