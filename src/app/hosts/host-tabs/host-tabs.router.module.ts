import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HostTabsPage } from './host-tabs.page';

const routes: Routes = [
	{
		path: 'host-tabs',
		component: HostTabsPage,
		children: [
			{
				path: 'listings',
				children: [
					{
						path: '',
						loadChildren: () =>
							import('../listings/listings.module').then(
								m => m.ListingsPageModule
							)
					}
				]
			},
			{
				path: 'host-chats',
				children: [
					{
						path: '',
						loadChildren: () =>
							import('../host-chats/host-chats.module').then(
								m => m.HostChatsPageModule
							)
					}
				]
			},
			{
				path: 'stats',
				children: [
					{
						path: '',
						loadChildren: () =>
							import('../stats/stats.module').then(m => m.StatsPageModule)
					}
				]
			},
			{
				path: 'profile',
				children: [
					{
						path: '',
						loadChildren: () =>
							import('../../clients/profile/profile.module').then(
								m => m.ProfilePageModule
							)
					}
				]
			},
			{
				path: '',
				redirectTo: '/host-tabs/listings',
				pathMatch: 'full'
			}
		]
	},
	{
		path: '',
		redirectTo: '/host-tabs',
		pathMatch: 'full'
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class HostTabsPageRoutingModule {}
