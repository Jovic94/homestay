import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { HostTabsPage } from './host-tabs.page';
import { HostTabsPageRoutingModule } from './host-tabs.router.module';

@NgModule({
	imports: [CommonModule, FormsModule, IonicModule, HostTabsPageRoutingModule],
	declarations: [HostTabsPage]
})
export class HostTabsPageModule {}
