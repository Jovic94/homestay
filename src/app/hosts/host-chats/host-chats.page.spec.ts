import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HostChatsPage } from './host-chats.page';

describe('HostChatsPage', () => {
  let component: HostChatsPage;
  let fixture: ComponentFixture<HostChatsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HostChatsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostChatsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
