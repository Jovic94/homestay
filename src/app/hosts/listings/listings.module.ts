import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ListingsPage } from './listings.page';

const routes: Routes = [
	{
		path: '',
		component: ListingsPage
	},
	{
		path: 'add-listing',
		loadChildren: () =>
			import('./add-listing/add-listing.module').then(
				m => m.AddListingPageModule
			)
	},
	{
		path: 'my-listing-details',
		loadChildren: () =>
			import('./my-listing-details/my-listing-details.module').then(
				m => m.MyListingDetailsPageModule
			)
	}
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes)
	],
	declarations: [ListingsPage]
})
export class ListingsPageModule {}
