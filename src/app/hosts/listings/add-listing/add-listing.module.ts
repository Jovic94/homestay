import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { AddListingPage } from './add-listing.page';
import { MatStepperModule } from '@angular/material';
import { AgmCoreModule } from '@agm/core';
const routes: Routes = [
	{
		path: '',
		component: AddListingPage
	}
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		MatStepperModule,
		ReactiveFormsModule,
		AgmCoreModule
	],
	declarations: [AddListingPage]
})
export class AddListingPageModule {}
