import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { Listing, Location, Coordinates } from 'src/app/models/listing';
import {
	FormGroup,
	FormControl,
	FormArray,
	NgForm,
	FormBuilder,
	Validators
} from '@angular/forms';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {
	Geocoder,
	GeocoderResult,
	GoogleMap,
	GoogleMapOptions,
	GoogleMaps,
	GoogleMapsEvent,
	LatLng,
	MarkerOptions,
	Environment
} from '@ionic-native/google-maps';
import {
	Platform,
	LoadingController,
	ActionSheetController,
	ToastController
} from '@ionic/angular';
import * as firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { ListingService } from 'src/app/services/listing.service';
import { Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { MapsAPILoader } from '@agm/core';
import { MatHorizontalStepper } from '@angular/material';

@Component({
	selector: 'app-add-listing',
	templateUrl: './add-listing.page.html',
	styleUrls: ['./add-listing.page.scss'],
	providers: [Geolocation, Camera]
})
export class AddListingPage implements OnInit {
	loading: any;
	listing: Listing = {};
	listingLocation: Location = {};
	coordinates: Coordinates = {};
	ammenityForm: FormGroup;
	selectedAmmenitiesNames = [];
	myAmmenities: [any];
	ammenities: any = [
		{
			name: 'Essentials',
			value: 'Essentials',
			description: 'Towels, bedsheets, soap, toilet paper, pillows',
			selected: true
		},
		{
			name: 'Internet Connection',
			value: 'Internet',
			description: 'Relatively fast internet'
		},
		{
			name: 'Kitchen',
			value: 'Kitchen',
			description: 'Guests are allowed to make their own meals'
		},
		{
			name: 'Parking',
			value: 'Parking',
			description: 'Free parking space for guests'
		},
		{
			name: 'TV',
			value: 'Tv',
			description: 'Television set for entertainment'
		},
		{
			name: 'Hot Shower',
			value: 'Hot Shower',
			description: 'Hot showers available'
		},
		{
			name: 'Washing Machine',
			value: 'Washing Machine',
			description: 'For laundry'
		},
		{
			name: 'Swimming Pool',
			value: 'Swimming Pool',
			description: ''
		},
		{
			name: 'Breakfast',
			value: 'Breakfast',
			description: 'Morning Meal'
		},
		{
			name: 'Gym',
			value: 'Gym',
			description: 'Fitness training section'
		},
		{
			name: 'Fireplace',
			value: 'Fireplace',
			description: 'For Keeping the space warm'
		}
	];
	lat: number;
	lng: number;
	map: GoogleMap;

	galleryRef: any;
	public photos: Photo[] = [];
	@ViewChild('stepper', { static: true })
	stepper: MatHorizontalStepper;
	@ViewChild('piStepper', { static: true }) piStepper: NgForm;
	@ViewChild('locStepper', { static: true }) locStepper: NgForm;
	@ViewChild('aiStepper', { static: true }) aiStepper: NgForm;
	@ViewChild('billingStepper', { static: true }) billingStepper: NgForm;

	// forms
	basicForm: FormGroup;
	guestForm: FormGroup;
	amenityForm: FormGroup;
	locationForm: FormGroup;
	galleryForm: FormGroup;
	cancellationForm: FormGroup;
	extrasForm: FormGroup;
	billingForm: FormGroup;

	submitAttempt = false;
	constructor(
		private geolocation: Geolocation,
		private platform: Platform,
		private afAuth: AngularFireAuth,
		private listingService: ListingService,
		private router: Router,
		private camera: Camera,
		private loadingCtrl: LoadingController,
		private actionSheetCtrl: ActionSheetController,
		private mapsAPILoader: MapsAPILoader,
		private zone: NgZone,
		private toastCtrl: ToastController,
		private formBuilder: FormBuilder
	) {
		this.createForms();
		this.galleryRef = firebase.storage().ref('/Gallery');
		afAuth.authState.subscribe(res => {
			if (res) {
				this.listing.uid = res.uid;
			}
		});
	}

	ngOnInit() {
		this.createAmenitiesFormInputs();
		// this.platform.ready().then(async () => {
		// 	await this.loadMap();
		// });
		// this.loadMap();
		this.autocompleteLocation();
	}

	createAmenitiesFormInputs() {
		this.ammenityForm = new FormGroup({
			ammenities: this.createAmmenities(this.ammenities)
		});
		this.getSelectedAmmenities('one,', 'two');
	}

	createAmmenities(ammenitiesInputs) {
		const arr = ammenitiesInputs.map(ammenity => {
			return new FormControl(ammenity.selected || false);
		});
		return new FormArray(arr);
	}

	getSelectedAmmenities(one, two) {
		this.selectedAmmenitiesNames = this.ammenityForm.controls.ammenities[
			'controls'
		].map((ammenity, i) => {
			return ammenity.value && this.ammenities[i].value;
		});
		this.getSelectedAmmenitiesName();
	}

	getSelectedAmmenitiesName() {
		this.selectedAmmenitiesNames = this.selectedAmmenitiesNames.filter(
			ammenity => {
				if (ammenity !== false) {
					return ammenity;
				}
			}
		);
	}

	/**
	 * @description :: Decrease (Guest Information)
	 */
	minus(info) {
		switch (info) {
			case 'Guests':
				this.guestForm.get('number_of_guests').value !== 0
					? this.guestForm
							.get('number_of_guests')
							.setValue((this.guestForm.value.number_of_guests -= 1))
					: null;
				break;
			case 'Bedrooms':
				this.guestForm.get('number_of_bedrooms').value !== 0
					? this.guestForm
							.get('number_of_bedrooms')
							.setValue((this.guestForm.value.number_of_bedrooms -= 1))
					: null;
				break;
			case 'Beds':
				this.guestForm.get('number_of_beds').value !== 0
					? this.guestForm
							.get('number_of_beds')
							.setValue((this.guestForm.value.number_of_beds -= 1))
					: null;
				break;
			case 'Bathrooms':
				this.guestForm.get('number_of_bathrooms').value !== 0
					? this.guestForm
							.get('number_of_bathrooms')
							.setValue((this.guestForm.value.number_of_bathrooms -= 1))
					: null;
				break;

			default:
				break;
		}
	}

	/**
	 * @description:: Increase (Guest Information)
	 */
	increase(info) {
		switch (info) {
			case 'Guests':
				this.guestForm
					.get('number_of_guests')
					.setValue((this.guestForm.value.number_of_guests += 1));
				break;
			case 'Bedrooms':
				this.guestForm
					.get('number_of_bedrooms')
					.setValue((this.guestForm.value.number_of_bedrooms += 1));
				break;
			case 'Beds':
				this.guestForm
					.get('number_of_beds')
					.setValue((this.guestForm.value.number_of_beds += 1));
				break;
			case 'Bathrooms':
				this.guestForm
					.get('number_of_bathrooms')
					.setValue((this.guestForm.value.number_of_bathrooms += 1));
				break;
			default:
				break;
		}
	}

	/**
	 * Inititalize map
	 */
	async loadMap() {
		// This code is necessary for browser
		Environment.setEnv({
			API_KEY_FOR_BROWSER_RELEASE: 'AIzaSyDdvzbaOKbs45Xn30JvBUJzjd62XfmCv9c',
			API_KEY_FOR_BROWSER_DEBUG: 'AIzaSyDdvzbaOKbs45Xn30JvBUJzjd62XfmCv9c'
		});
		this.map = await GoogleMaps.create('map');
		this.map.one(GoogleMapsEvent.MAP_READY).then(async (data: any) => {
			this.getLocation();
		});
	}

	/**
	 * Get automatic user location
	 */
	getLocation() {
		this.geolocation
			.getCurrentPosition({ enableHighAccuracy: true })
			.then(async position => {
				this.coordinates.lat = await position.coords.latitude;
				this.coordinates.lng = await position.coords.longitude;
				const latlng: LatLng = new LatLng(
					this.coordinates.lat,
					this.coordinates.lng
				);
				const markerOptions: MarkerOptions = {
					position: latlng,
					icon: 'assets/icon/home_icon.png',
					animation: 'BOUNCE'
				};
				const mapOptions: GoogleMapOptions = {
					camera: {
						target: {
							lat: this.coordinates.lat,
							lng: this.coordinates.lng
						},
						zoom: 14,
						tilt: 30
					}
				};
				this.map.setOptions(mapOptions);
				return this.map
					.addMarker(markerOptions)
					.then(() => {
						// Geocode to get user address
					})
					.catch(err => console.log(err));
			})
			.catch(err => {
				console.log(err);
			});
	}

	/**
	 * @description :: Create Listing
	 */
	async createListing() {
		if (this.billingStepper.form.valid) {
			this.loader();
			this.listing.amenities = this.selectedAmmenitiesNames;
			this.listing.is_booked = false;
			this.listing.is_popular = false;
			this.listing.created_at = new Date().toDateString();
			this.listing.rating = 0;
			const loc = {
				location: { ...this.listingLocation, ...this.coordinates }
			};
			const finalListing = {
				...this.listing,
				...loc,
				...this.basicForm.value,
				...this.guestForm.value
			};
			const listing = await this.listingService.createListing(finalListing);
			const listingId = listing.id;
			const photos = this.photos;
			const pics = [];
			for (let i = 0; 0 < photos.length; i++) {
				const imageRef = await this.galleryRef.child(`/${listingId}${i}.jpg`);
				const snapshot = await imageRef.putString(
					photos[i].data,
					firebase.storage.StringFormat.DATA_URL
				);
				const picture = await firebase
					.storage()
					.ref(`/Gallery/${listingId}${i}.jpg`)
					.getDownloadURL();
				pics.push(picture);
				if (i + 1 === photos.length) {
					await this.listingService.updateListing(listingId, pics);
					// await this.loading.dismiss();
					return this.router.navigate(['/host/host-tabs/listings']);
				}
			}
		} else {
			this.toastr('Please fill in the required field');
		}
	}

	/**
	 * @description :: Function prompts action sheet for loading an image from gallery or using the camera to take pictures
	 */
	async uploadGallery() {
		const actionSheet = await this.actionSheetCtrl.create({
			buttons: [
				{
					text: 'Load from gallery',
					handler: () => {
						this.loadImages(this.camera.PictureSourceType.PHOTOLIBRARY);
					}
				},
				{
					text: 'Take a photo',
					handler: () => {
						this.loadImages(this.camera.PictureSourceType.CAMERA);
					}
				},
				{
					text: 'Cancel',
					role: 'cancel'
				}
			]
		});
		await actionSheet.present();
	}

	/**
	 * @description :: Load images to be uploaded
	 */
	private loadImages(selectedSourceType: number) {
		const cameraOptions: CameraOptions = {
			sourceType: selectedSourceType,
			destinationType: this.camera.DestinationType.DATA_URL,
			quality: 100,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE,
			correctOrientation: true
		};
		this.camera
			.getPicture(cameraOptions)
			.then(imageData => {
				if (imageData != null) {
					// Do with the image data what you want.
					this.photos.unshift({
						data: 'data:image/jpeg;base64,' + imageData
					});
				}
			})
			.catch(err => {
				window.alert(JSON.stringify(err));
			});
	}

	async loader() {
		this.loading = await this.loadingCtrl.create({
			spinner: 'crescent',
			message: 'Creating listing ...',
			translucent: true
		});
		return await this.loading.present();
	}

	async toastr(message) {
		const toast = await this.toastCtrl.create({
			message,
			duration: 4000,
			position: 'top'
		});
		return await toast.present();
	}

	autocompleteLocation() {
		//load Places Autocomplete
		this.mapsAPILoader.load().then(() => {
			const autocomplete = new google.maps.places.Autocomplete(
				<HTMLInputElement>document.getElementById('location'),
				{
					componentRestrictions: { country: 'ke' }
				}
			);
			autocomplete.addListener('place_changed', () => {
				this.zone.run(() => {
					//get the place result
					const place: google.maps.places.PlaceResult = autocomplete.getPlace();
					//verify result
					if (place.geometry === undefined || place.geometry === null) {
						return;
					}

					console.log(place);

					this.coordinates.lat = place.geometry.location.lat();
					this.coordinates.lng = place.geometry.location.lng();
					for (let i in place.address_components) {
						let item = place.address_components[i];
						this.listingLocation.address = place.formatted_address;
						if (item['types'].indexOf('locality') > -1) {
							this.listingLocation.town = item['long_name'];
						} else if (
							item['types'].indexOf('administrative_area_level_1') > -1
						) {
							this.listingLocation.county = item['short_name'];
						} else if (item['types'].indexOf('street_number') > -1) {
						} else if (item['types'].indexOf('route') > -1) {
							this.listingLocation.route = item['short_name'];
						} else if (item['types'].indexOf('country') > -1) {
							this.listingLocation.country = item['long_name'];
						} else if (item['types'].indexOf('postal_code') > -1) {
						}
					}
				});
			});
		});
	}

	nextStep(val) {
		this.submitAttempt = true;
		val === 'pi' && this.basicForm.valid
			? this.stepper.next()
			: val === 'gi' && this.guestForm.valid
			? this.stepper.next()
			: val === 'loc' && this.locStepper.form.valid
			? this.stepper.next()
			: val === 'ai' && this.locStepper.form.valid
			? this.stepper.next()
			: this.toastr('Please fill in the required fields');
	}

	createForms() {
		this.basicForm = this.formBuilder.group({
			property_name: [
				'',
				Validators.compose([
					Validators.maxLength(30),
					Validators.pattern('[a-zA-Z ]*'),
					Validators.required
				])
			],
			property_type: ['', Validators.required],
			rentable_space: ['', Validators.required],
			description: ['']
		});

		this.guestForm = this.formBuilder.group({
			number_of_guests: [0, Validators.required],
			number_of_bedrooms: [0, Validators.required],
			number_of_beds: [0, Validators.required],
			number_of_bathrooms: [0, Validators.required]
		});
	}
}

class Photo {
	data: any;
}
