import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddListingPage } from './add-listing.page';

describe('AddListingPage', () => {
  let component: AddListingPage;
  let fixture: ComponentFixture<AddListingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddListingPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddListingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
