import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { ListingService } from 'src/app/services/listing.service';

@Component({
	selector: 'app-listings',
	templateUrl: './listings.page.html',
	styleUrls: ['./listings.page.scss']
})
export class ListingsPage implements OnInit {
	currentIndex: any;
	uid: any;
	myListings: any;
	@ViewChild('mySlider', { static: true }) slider: IonSlides;
	constructor(
		private afAuth: AngularFireAuth,
		private listingService: ListingService
	) {
		afAuth.authState.subscribe(res => {
			if (res) {
				const uid = res.uid;
				this.getMyListings(uid);
			}
		});
	}

	ngOnInit() {}

	async onSlideChanged() {
		this.currentIndex = await this.slider.getActiveIndex();
		if (this.currentIndex === 3) {
			this.slider.lockSwipes(true);
		}
	}

	getMyListings(uid) {
		this.listingService.getUserListings(uid).subscribe(res => {
			this.myListings = res;
		});
	}
}
