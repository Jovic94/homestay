import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MyListingDetailsPage } from './my-listing-details.page';

const routes: Routes = [
  {
    path: '',
    component: MyListingDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MyListingDetailsPage]
})
export class MyListingDetailsPageModule {}
