import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Listing } from 'src/app/models/listing';
import { ListingService } from 'src/app/services/listing.service';
import { ActionSheetController } from '@ionic/angular';

@Component({
	selector: 'app-my-listing-details',
	templateUrl: './my-listing-details.page.html',
	styleUrls: ['./my-listing-details.page.scss']
})
export class MyListingDetailsPage implements OnInit {
	listing: any;
	listingId: any;
	constructor(
		private route: ActivatedRoute,
		private listingService: ListingService,
		private actionSheetCtrl: ActionSheetController
	) {
		this.listingId = route.snapshot.queryParamMap.get('id');
	}

	async ngOnInit() {
		await this.getListingDetails(this.listingId);
	}

	getListingDetails(id) {
		this.listingService.getListing(id).subscribe(res => {
			this.listing = res;
			console.log(res);
		});
	}

	async actions() {
		const actionSheet = await this.actionSheetCtrl.create({
			header: 'Actions',
			buttons: [
				{
					text: 'Share',
					icon: 'share',
					handler: () => {
						console.log('Share clicked');
					}
				},
				{
					text: 'Edit',
					icon: 'create',
					handler: () => {
						console.log('Play clicked');
					}
				},
				{
					text: 'Delete',
					role: 'destructive',
					icon: 'trash',
					handler: () => {
						console.log('Delete clicked');
					}
				},

				{
					text: 'Cancel',
					icon: 'close',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				}
			]
		});
		await actionSheet.present();
	}
}
