export interface Listing {
	id?: string;
	uid?: string;
	property_name?: string;
	price_per_night?: string;
	property_type?: string;
	description?: string;
	rentable_space?: string;
	number_of_guests?: number;
	number_of_bedrooms?: number;
	number_of_beds?: number;
	number_of_bathrooms?: number;
	amenities?: any;
	gallery?: any;
	contact_person_name?: string;
	contact_number?: string;
	national_id?: string;
	availability_status?: boolean;
	rating?: number;
	is_popular?: boolean;
	landmark?: string;
	is_booked?: boolean;
	created_at?: string;
	updated_at?: string;
	archived_at?: string;
}

export interface Location {
	country?: string;
	county?: string;
	town?: string;
	route?: string;
	address?: string;
}

export interface Coordinates {
	lat?: number;
	lng?: number;
}
