export interface User {
	uid?: string;
	avatar_url?: string;
	email?: string;
	first_name?: string;
	last_name?: string;
	phone_number?: string;
	gender?: string;
	status?: string;
	d_o_b?: string;
	national_id?: string;
	created_at?: string;
	updated_at?: string;
	archived_at?: string;
	is_host?: boolean;
}
